import { getDate } from './utils';
import { v4 as uuidv4 } from 'uuid';

export const setChatData = (state, { chatName, messages, user }) => {
  return {
    ...state,
    chatName,
    messages,
    user,
    isLoading: false
  };
};

export const updateMessageLikes = (state, { id }) => {
  const updatedMessages = state.messages.map(msg => {
    if (msg.id !== id) {
      return msg;
    }

    let { likes, isLikedByCurrentUser } = msg;

    if (isLikedByCurrentUser) {
      return {
        ...msg,
        likes: likes - 1,
        isLikedByCurrentUser: false
      };
    }

    return {
      ...msg,
      likes: likes + 1,
      isLikedByCurrentUser: true
    };
  });

  return {
    ...state,
    messages: updatedMessages
  };
};

export const editMessage = (state, { id, text }) => {
  const updatedMessages = state.messages.map(msg => {
    if (msg.id !== id) {
      return msg;
    }

    return {
      ...msg,
      text,
      editedAt: new Date().toISOString()
    };
  })

  return {
    ...state,
    messages: updatedMessages
  };
};

export const addMessage = (state, { text, currentUser }) => {
  const createdAt = new Date().toISOString();
  const [date, time] = getDate(createdAt);
  const { user, userId } = currentUser;

  const newMessage = {
    id: uuidv4(),
    text,
    user,
    userId,
    editedAt: '',
    createdAt,
    date,
    time,
    likes: 0,
  };

  return {
    ...state,
    messages: [...state.messages, newMessage]
  };
};

export const deleteMessage = (state, { id }) => ({
  ...state,
  messages: state.messages.filter((msg) => msg.id !== id)
});
