import moment from 'moment';

export const getDate = isoDate => {
  const date = moment(isoDate).format('ddd, MMM Do');
  const time = moment(isoDate).format('HH:mm');
  
  return [date, time];
}