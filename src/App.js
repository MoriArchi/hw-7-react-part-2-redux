import React from 'react';
import Chat from './containers/Chat';
import AppHeader from './components/AppHeader';
import AppFooter from './components/AppFooter';

const App = () => {
  return (
    <>
      <AppHeader />
      <Chat />
      <AppFooter />
    </>
  );
}

export default App;
