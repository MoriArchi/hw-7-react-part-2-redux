import {
  SHOW_EDIT_MESSAGE_POPUP,
  HIDE_EDIT_MESSAGE_POPUP,
} from '../actions/actionTypes';

const initState = {
  isEditingMode: false,
  id: '',
  text: '',
};

export default function editMessagePopupReducer(state = initState, { type, payload }) {
  switch (type) {
    case SHOW_EDIT_MESSAGE_POPUP:
      return {
        isEditingMode: true,
        id: payload.id,
        text: payload.text,
      }

    case HIDE_EDIT_MESSAGE_POPUP:
      return { ...initState };

    default:
      return state;
  }
}
