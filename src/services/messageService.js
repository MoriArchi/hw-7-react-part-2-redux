import moment from 'moment';
import { getDate } from '../helpers/utils';
import TIME_FORMAT from '../constants/timeFormat';

const API_URL = 'https://edikdolynskyi.github.io/react_sources/messages.json';

const getData = async url => {
  const res = await fetch(url);
  if (!res.ok) {
    throw new Error(`Received ${res.status}`);
  }
  return await res.json();
};

const createMessagesSortedByDate = data => {
  const messages = data.map(message => {
    const [date, time] = getDate(message.createdAt);

    return {
      ...message,
      isLikedByCurrentUser: false,
      likes: 0,
      date,
      time,
    };
  });
  
  return messages
    .sort(({ createdAt }, { createdAt: secondCreatedAt }) => {
      return moment(createdAt, TIME_FORMAT).diff(secondCreatedAt, TIME_FORMAT);
    });
};

const getMessages = async url => {
  const data = await getData(url);
  const messages = createMessagesSortedByDate(data);
  return messages;
};

const createUser = () => {
  return {
    user: 'Helen',
    avatar: 'https://resizing.flixster.com/PCEX63VBu7wVvdt9Eq-FrTI6d_4=/300x300/v1.cjs0MzYxNjtqOzE4NDk1OzEyMDA7MzQ5OzMxMQ',
    userId: '4b003c20-1b8f-11e8-9629-c7eca82aa7bd'
  };
};

const getChatName = () => 'React-redux chat';

const getChatData = async () => {
  const messages = await getMessages(API_URL);
  const user = createUser();
  const chatName = getChatName();
  return { chatName, messages, user };
};

export default getChatData;