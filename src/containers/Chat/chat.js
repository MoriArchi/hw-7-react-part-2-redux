import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import Loader from '../../components/Loader';
import MessageListContaner from '../MessageListContainer';
import MessageInputContainer from '../MessageInputContainer';
import MessageEditPopupContainer from '../MessageEditPopupContainer';
import HeaderConainer from '../HeaderContainer';
import { loadChatData } from '../../actions';
import { isLoadingSelector } from '../../selectors';
import './chat.css';

class Chat extends Component {
  componentDidMount() {
    this.props.onLoadChatData();
  }

  render() {
    const { isLoading } = this.props;

    if (isLoading) {
      return <Loader />;
    }

    return (
      <div className="chat">
        <HeaderConainer />
        <MessageListContaner />
        <MessageInputContainer />
        <MessageEditPopupContainer />
      </div>
    );
  }
}

const mapStateToProps = state => ({
  isLoading: isLoadingSelector(state),
});

const mapDispatchToProps = {
  onLoadChatData: loadChatData
};

export default connect(mapStateToProps, mapDispatchToProps)(Chat);

Chat.propTypes = {
  onLoadChatData: PropTypes.func.isRequired,
  isLoading: PropTypes.bool.isRequired,
};
