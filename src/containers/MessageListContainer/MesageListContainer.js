

import React, { useCallback, useEffect, useRef } from 'react';
import { useSelector } from 'react-redux';

import useActions from '../../helpers/useActions';
import MessageList from '../../components/MessageList';
import { getUserLastMessage } from '../../helpers/chatHelper';
import { currentUserIdSelector, messagesSelector } from '../../selectors';
import { updateMessageLikes, deleteMessage, showEditMessagePopup } from '../../actions';

function MessageListContaner() {
  const messages = useSelector(messagesSelector);
  const currentUserId = useSelector(currentUserIdSelector);

  const [
    onMessageLike,
    onMessageDelete,
    onShowEditMessagePopup
  ] = useActions(
    updateMessageLikes,
    deleteMessage,
    showEditMessagePopup
  );

  const messagesBottomRef = useRef();

  const scrollToBottom = useCallback(() => {
    messagesBottomRef.current && messagesBottomRef.current.scrollIntoView({ behavior: 'smooth' });
  }, [messagesBottomRef]);

  const handleEditLastMessage = useCallback(event => {
    if (event.keyCode === 38) {
      const { isMessageExist, id, text } = getUserLastMessage(messages, currentUserId);

      if (isMessageExist) {
        onShowEditMessagePopup({ id, text });
      }
    }
  }, [onShowEditMessagePopup, messages, currentUserId]);

  useEffect(() => {
    document.addEventListener('keydown', handleEditLastMessage);
    return () => {
      document.removeEventListener('keydown', handleEditLastMessage);
    }
  }, [handleEditLastMessage]);

  const messagesCount = messages.length;

  useEffect(() => {
    scrollToBottom();
  }, [messagesCount, scrollToBottom]);

  return (
    <MessageList
      messages={messages}
      currentUserId={currentUserId}
      onMessageLike={onMessageLike}
      onMessageDelete={onMessageDelete}
      messagesBottomRef={messagesBottomRef}
      onShowEditMessagePopup={onShowEditMessagePopup}
    />
  )
}

export default MessageListContaner;