import React, { useCallback } from 'react';
import { connect, useSelector } from 'react-redux';

import MessageInput from '../../components/MessageInput';
import useActions from '../../helpers/useActions';
import { addMessage } from '../../actions';
import { currentUserSelector } from '../../selectors';

function MessageInputContainer() {
  const currentUser = useSelector(currentUserSelector);
  const [onAddMessage] = useActions(addMessage);

  const handleAddMessage = useCallback((text) => {
    onAddMessage(text, currentUser)
  }, [onAddMessage, currentUser]);

  return (
    <MessageInput onAddMessage={handleAddMessage} />
  );
}

const mapDispatchToProps = {
  onAddMessage: addMessage
};

export default connect(null, mapDispatchToProps)(MessageInputContainer);
