import React from 'react';
import { connect } from 'react-redux';

import MessageEditPopup from '../../components/MessageEditPopup';
import { editMessagePopupSelector } from '../../selectors';
import { hideEditMessagePopup, editMessage } from '../../actions';

function MessageEditPopupContainer({
  editMessagePopupProps,
  onEditMessage,
  onHidePopup,
}) {

  return (
    <MessageEditPopup
      {...editMessagePopupProps}
      onHidePopup={onHidePopup}
      onEditMessage={onEditMessage}
    />
  );
}

const mapStateToProps = state => ({
  editMessagePopupProps: editMessagePopupSelector(state),
});

const mapDispatchToProps = {
  onHidePopup: hideEditMessagePopup,
  onEditMessage: editMessage,
};

export default connect(mapStateToProps, mapDispatchToProps)(MessageEditPopupContainer);