import {
  SET_CHAT_DATA,
  UPDATE_MESSAGE_LIKES,
  SHOW_EDIT_MESSAGE_POPUP,
  HIDE_EDIT_MESSAGE_POPUP,
  EDIT_MESSAGE,
  ADD_MESSAGE,
  DELETE_MESSAGE
} from './actionTypes';

import getChatData from '../services/messageService';

const setChatData = ({ chatName, messages, user }) => {
  return {
    type: SET_CHAT_DATA,
    payload: { chatName, messages, user }
  };
};

export const loadChatData = () => async dispatch => {
  const chatData = await getChatData();
  dispatch(setChatData(chatData));
};

export const updateMessageLikes = id => {
  return {
    type: UPDATE_MESSAGE_LIKES,
    payload: { id }
  };
};

export const showEditMessagePopup = ({ id, text }) => ({
  type: SHOW_EDIT_MESSAGE_POPUP,
  payload: {id, text }
});

export const hideEditMessagePopup = () => ({
  type: HIDE_EDIT_MESSAGE_POPUP
});

export const editMessage = ({ isEdit, id, text }) => ({
  type: EDIT_MESSAGE,
  payload: {
    isEdit,
    id,
    text
  }
});

export const addMessage = (text, currentUser) => ({
  type: ADD_MESSAGE,
  payload: { text, currentUser }
});

export const deleteMessage = id => ({
  type: DELETE_MESSAGE,
  payload: { id }
});
