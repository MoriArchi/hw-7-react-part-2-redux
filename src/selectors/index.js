import { getPartipiantsCount, getLastMessageTime } from '../helpers/chatHelper';

export const messagesSelector = state => state.chat.messages;
export const isLoadingSelector = state => state.chat.isLoading;
export const currentUserSelector = state => state.chat.user;
export const currentUserIdSelector = state => currentUserSelector(state).userId;
export const messagesCountSelector = state => messagesSelector(state).length;

export function partipiantsCountSelector(state) {
  const messages = messagesSelector(state);
  return getPartipiantsCount(messages);
};

export function lastMessageTimeSelector(state) {
  const messages = messagesSelector(state);
  return getLastMessageTime(messages);
}

export const chatNameSelector = state => state.chat.chatName;
export const editMessagePopupSelector = state => state.editMessagePopup;
