import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';

import './message.css';

const Message = ({
  isLikedByCurrentUser,
  onShowEditPopup,
  isOwnMessage,
  editedAt,
  onDelete,
  onLike,
  avatar,
  likes,
  text,
  user,
  time,
}) => {
  const messageClassNames = isOwnMessage ? 'message message--own' : 'message message--users';
  const likeClassNames = isLikedByCurrentUser ? 'message__like message__like--active' : 'message__like';
  const editedAtLabel = editedAt ? `edited at ${moment(editedAt).format('Do MMMM HH:mm')}` : '';

  // todo: display username somewhere

  return (
    <div className={messageClassNames}>
      <div className="message__body">
        {!isOwnMessage && <img className="message__avatar" src={avatar} alt={user} />}
        <div className="message__content">
          <p className="message__text">{text}</p>
          <p className="message__time">
            {time} {editedAtLabel}
          </p>
        </div>
      </div>
      <div className="message__controls">
        <span className={likeClassNames}>
          <i className="fa fa-heart" aria-hidden="true" onClick={isOwnMessage ? null : onLike} />
          {likes !== 0 ? likes : null}
        </span>
        {isOwnMessage && (
          <>
            <span className="message__edit">
              <i className="fa fa-cog" aria-hidden="true" onClick={onShowEditPopup}></i>
            </span>
            <span className="message__delete">
              <i className="fa fa-trash" aria-hidden="true" onClick={onDelete}></i>
            </span>
          </>
        )}
      </div>
    </div>
  );
};

Message.propTypes = {
  text: PropTypes.string.isRequired,
  user: PropTypes.string.isRequired,
  avatar: PropTypes.string.isRequired,
  time: PropTypes.string.isRequired,
  editedAt: PropTypes.string.isRequired,
  isLikedByCurrentUser: PropTypes.bool.isRequired,
  isOwnMessage: PropTypes.bool.isRequired,
  onLike: PropTypes.func.isRequired,
  onDelete: PropTypes.func.isRequired,
  onShowEditPopup: PropTypes.func.isRequired,
};

export default Message;
