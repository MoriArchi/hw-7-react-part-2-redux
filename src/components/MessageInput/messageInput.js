import React, { Component } from 'react';
import PropTypes from 'prop-types';

import './messageInput.css';

class MessageInput extends Component {
  state = {
    value: ''
  };

  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange({ target }) {
    this.setState({ value: target.value });
  };

  handleSubmit(event) {
    event.preventDefault();
    const { value } = this.state;

    if (!value) {
      return;
    }

    const { onAddMessage } = this.props;
    onAddMessage(value);

    this.setState({ value: '' });
  };

  render() {
    return (
      <div className="message-input">
        <form onSubmit={this.handleSubmit}>
          <textarea
            placeholder="Type your message here..."
            onChange={this.handleChange}
            value={this.state.value}
          />
          <div className="message-input__buttons">
            <button type="submit">Send</button>
          </div>
        </form>
      </div>
    );
  }
}

MessageInput.propTypes = {
  onAddMessage: PropTypes.func.isRequired
};

export default MessageInput;

