import React, { Component } from 'react';
import PropTypes from 'prop-types';

import './messageEditPopup.css';

class MessageEditPopup extends Component {
  state = {
    value: ''
  };

  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentDidUpdate(prevProps) {
    const { isEditingMode, text } = this.props;

    if (prevProps.isEditingMode !== isEditingMode) {
      this.setState({ value: text });
    }
  }

  handleChange({ target }) {
    const value = target.value;
    this.setState({ value });
  };

  handleSubmit(event) {
    event.preventDefault();
    const { value } = this.state;
    const { onHidePopup, onEditMessage, id } = this.props;

    const formatedValue = value.trim();

    if (!formatedValue) {
      return;
    }

    onEditMessage({ text: formatedValue, id });
    onHidePopup();
  };

  render() {
    const { isEditingMode, onHidePopup } = this.props;

    if (!isEditingMode) {
      return null;
    }

    return (
      <div className="modal-container">
        <div className="modal-wrapper">
          <div className="modal">
            <h3 className="modal__header">Edit message</h3>
            <form onSubmit={this.handleSubmit}>
              <div className="modal__content">
                <textarea
                  autoFocus
                  placeholder="Type your edited message here..."
                  onChange={this.handleChange}
                  value={this.state.value}
                />
              </div>
              <div className="modal__buttons">
                <button className="modal__button" type="submit">
                  Update
                </button>
                <button className="modal__button modal__button--cancel" onClick={onHidePopup}>
                  Cancel
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default MessageEditPopup;

MessageEditPopup.propTypes = {
  isEditingMode: PropTypes.bool.isRequired,
  id: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired,
  onHidePopup: PropTypes.func.isRequired,
  onEditMessage: PropTypes.func.isRequired
};
